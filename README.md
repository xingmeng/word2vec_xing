# README #

1. preprocessing_forworkingdataset.py
	-- script for preprocessing the original corpus, basically, it: 
	* lower cased all letters
	* removed all numbers
	* removed almost all punctuations, only leaving :. \'
	* replaced '.' with ' puncdot ', and replaced ':' with ' punccolon '

2. tensorflow_example_punc.py
	-- script for training word2vec model, the model's:
	* dimension size = 300 
	* window size = 8 

3. vecDict.txt
	-- the word2vec model I trained
	* simply call w2v["word"] to get any vector for a "word"