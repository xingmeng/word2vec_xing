# pre-processing all docs

import os
import glob
import re
import logging


rule = re.compile("[^a-z:. \']")


path = "/pool1/users/xing/spring18/data/newdata/commu/"

logging.basicConfig(filename="/pool1/users/xing/spring18/code/getnewdata/getnewdata.log", level=logging.INFO)

newpath = "/pool1/users/xing/spring18/data/workingdataset/commu/"

for filename in os.listdir(path):
    if filename.endswith(".txt"):
        with open(path + filename,'r') as f_doc:
            f_content = f_doc.read().decode('utf8').lower().replace(u'\u2011','').replace(u'\u2019',"'").replace(u'\u2014','').replace(u'\xfc','').replace("\r\n"," ").replace("\n"," ").replace("\r"," ").replace("\t"," ")
            try:
                f_content = rule.sub('',str(f_content))
            except Exception as e:
                logging.error("%s-------------%s",filename,e)
                continue
            f_content = f_content.replace('.',' puncdot ').replace(':',' punccolon ')
            # print f_content
            with open(newpath + filename,'w') as f_new:          
                f_new.write(f_content.encode('utf-8'))

logging.info("----------------Done!----------------")    
